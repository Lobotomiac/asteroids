// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AsteroidsGameMode.h"
#include "Player/PlayerPawn.h"
#include "HighScore.h"
#include "ScoreList.h"

#include "Kismet/GameplayStatics.h"



// TODO Get Pause menu working

AAsteroidsGameMode::AAsteroidsGameMode() : Super()
{
	PrimaryActorTick.bCanEverTick = false;
	// set default pawn class to our character class
	DefaultPawnClass = APlayerPawn::StaticClass();
	
	// Making sure the score is 0 as a safeguard against dying without doing anything
	PlayerScore = 0;
}


void AAsteroidsGameMode::BeginPlay()
{
	// Creating an instance of our SaveGame class
	SaveGameInstance = Cast<UHighScore>(UGameplayStatics::CreateSaveGameObject(UHighScore::StaticClass()));

	// Set the GameState as currently playing (even tho we don't use GameState class its still a good thing to do)
	SetCurrentState(EGamePlayState::EPlaying);

	// Get the player pawn for use throughout the game
	MyPlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));

	Super::BeginPlay();
}


EGamePlayState AAsteroidsGameMode::GetCurrentState() const { return CurrentState; }


void AAsteroidsGameMode::SetCurrentState(EGamePlayState NewState)
{
	CurrentState = NewState;
	HandleNewState(CurrentState);
}


void AAsteroidsGameMode::PlayerScoreUpdate(int32 Points)
{
	PlayerScore += Points;
}


int32 AAsteroidsGameMode::GetCurrentPlayerScore()
{
	return PlayerScore;
}


FText AAsteroidsGameMode::GetPlayerScoreIntAsText()
{
		FString HPString = FString::FromInt(GetCurrentPlayerScore());
		FText HPText = FText::FromString(HPString);
		return HPText;
}



void AAsteroidsGameMode::IsPlayerDead()
{
	if (FMath::IsNearlyZero(MyPlayerPawn->GetCurrentHitPoints(), 0.01f))
	{
		SetCurrentState(EGamePlayState::EGameOver);
	}
}


void AAsteroidsGameMode::IsEarthDestroyed(bool Destroyed)
{
	if (Destroyed)
	{
		SetCurrentState(EGamePlayState::EGameOver);
	}
}


void AAsteroidsGameMode::HandleNewState(EGamePlayState NewState)
{
	switch(NewState)
	{
		case EGamePlayState::EPlaying:
		{
			LoadScore();
		}
		break;

		case EGamePlayState::EGameOver:
		{
			ReceiveGameOver();
			// Pause the game, display score, open a menu to choose what to do (play again, quit) etc
			SaveScore();
			if (BPScoreList)
			{
				// Creating the Widget for Displaying the ScoreList to the screen
				ScoreList = CreateWidget<UScoreList>(UGameplayStatics::GetPlayerController(GetWorld(), 0), BPScoreList);
				if (ScoreList)
				{
					// Adds the Widget to the screen
					ScoreList->AddToViewport(50);
					// Making sure we see the mouse cursor in the widget and that buttons can be clicked
					APlayerController* MyController = GetWorld()->GetFirstPlayerController();
					MyController->bShowMouseCursor = true;
					MyController->bEnableClickEvents = true;

					// Making sure the Widget is in focus as soon as it is displayed ( we don't have to click on it to set it to focus like this )
					ScoreList->bIsFocusable = true;
					ScoreList->SetUserFocus(MyController);
					// Making sure the Input is only received on the widget itself and not the game in the background
					MyController->SetInputMode(FInputModeUIOnly());

					if (SaveGameInstance)
					{
						auto HighScores = SaveGameInstance->GetHighScoreList();	// Creating a copy of the HighScoreList array
						// Displaying each score to the UI
						for (auto Score : HighScores)
						{
							ScoreList->AddScoreToUI(Score);
						}
					}
				}
				else
				{
					UE_LOG(LogTemp, Error, TEXT("ScoreList NULL"))
				}
			}
		}
		break;

		default:
		case EGamePlayState::EUnknown:
		{
			// do nothing
		}
		break;
	}
}

bool AAsteroidsGameMode::SaveScore()
{
	
	// Adding the Current Score to the HighScoreList
	if (SaveGameInstance)
	{
		SaveGameInstance->UpdateHighScoreList(GetCurrentPlayerScore());
	}
	else { UE_LOG(LogTemp, Error, TEXT("HighScoreInstance doesn't exist")) }
	
	// Save the HighScoreInstance
	if (UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("HighScores"), 0))
	{
		UE_LOG(LogTemp, Warning, TEXT("Game Saved"))
		return true;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Game NOT Saved"))
		return false;
	}
}


bool AAsteroidsGameMode::LoadScore()
{
		// Trying to load our SaveGame class ( Load the scores ) 
	SaveGameInstance = Cast<UHighScore>(UGameplayStatics::LoadGameFromSlot("HighScores", 0));
	if (SaveGameInstance)
	{
		UE_LOG(LogTemp, Warning, TEXT("LoadScore(): Game Loaded!"))
		return true;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("LoadScore: Can't Load Game"))
	}
	return false;
}


