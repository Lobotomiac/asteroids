// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AsteroidsGameMode.generated.h"

class APlayerPawn; 
class UHighScore;
class UScoreList;


/* Set up the different states of play to know which screen to show */
UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUnknown
};


UCLASS(MinimalAPI)
class AAsteroidsGameMode : public AGameModeBase
{
	GENERATED_BODY()

	float PlayerScore;

public:
	AAsteroidsGameMode();

	virtual void BeginPlay() override;

	UPROPERTY()
	APlayerPawn* MyPlayerPawn = nullptr;

	UPROPERTY()
	UScoreList* ScoreList = nullptr;

	/*Property which is pointing to our Widget Blueprint in order to instantiate it using c++*/
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UScoreList> BPScoreList;

	UFUNCTION(BlueprintPure, Category = "Health")
	EGamePlayState GetCurrentState() const;

	UFUNCTION(BlueprintImplementableEvent, Category = "GameOver")
	void ReceiveGameOver();

	// Setting What state the game is in at the moment (playing, ended etc)
	void SetCurrentState(EGamePlayState NewState);
	void IsPlayerDead();
	void IsEarthDestroyed(bool bEarthDestroyed);
	void PlayerScoreUpdate(int32 Points);
	int32 GetCurrentPlayerScore();

	UFUNCTION(BlueprintPure, Category = "Score")
	FText GetPlayerScoreIntAsText();

	UPROPERTY(SaveGame)
	UHighScore* SaveGameInstance = nullptr;

private:
	EGamePlayState CurrentState;

	// Set the current state of the game
	void HandleNewState(EGamePlayState NewState);

	// saves the high score
	bool SaveScore();

	// Loads the high score
	bool LoadScore();
};
