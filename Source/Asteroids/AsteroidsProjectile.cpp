// Copyright 1998-2018 Epic Games, Inc. All Rights Reserve

#include "AsteroidsProjectile.h"
#include "Enemy/Asteroid.h"
#include "Enemy/ChargerShip.h"
#include "Player/PlayerPawn.h"

#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"
#include "Components/SphereComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"


// TODO change collision settings so as to not block PlayerPawn

AAsteroidsProjectile::AAsteroidsProjectile() 
{
	// Static reference to the mesh to use for the projectile
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ProjectileMeshAsset(TEXT("/Game/Meshes/TwinStickProjectile.TwinStickProjectile"));
	// Create mesh component for the projectile sphere
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh0"));
	ProjectileMesh->SetStaticMesh(ProjectileMeshAsset.Object);
	ProjectileMesh->BodyInstance.SetCollisionProfileName("CharacterMesh");
	ProjectileMesh->bGenerateOverlapEvents = true;
	ProjectileMesh->SetNotifyRigidBodyCollision(true);
	ProjectileMesh->OnComponentHit.AddDynamic(this, &AAsteroidsProjectile::OnHit);
	RootComponent = ProjectileMesh;


	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement0"));
	ProjectileMovement->UpdatedComponent = ProjectileMesh;
	ProjectileMovement->InitialSpeed = 2000.f;
	ProjectileMovement->MaxSpeed = 2000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = 0.f; // No gravity
	ProjectileMovement->OnProjectileStop.AddDynamic(this, &AAsteroidsProjectile::OnComponentHit);

	// Setting up the Physical force emiting component (pushes stuff around when fired)
	RadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	RadialForceComponent->SetupAttachment(RootComponent);
	RadialForceComponent->Radius = SphereRadius + 15;
	RadialForceComponent->ImpulseStrength = 100.0f;
	RadialForceComponent->bImpulseVelChange = false;
	RadialForceComponent->DestructibleDamage = ImpactDamage;
	
	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	// Setting up the starting damage
	ImpactDamage = 25.0f;

	// HitImpulseStrength
	HitImpulseStrength = 20.0f;
}


void AAsteroidsProjectile::BeginPlay()
{
	Super::BeginPlay();
}


// Blasting a radial impulse from the projectile before destroying it 
void AAsteroidsProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!(OtherActor->IsA(APlayerPawn::StaticClass())))
	{
		RadialForceComponent->FireImpulse();
		if (OtherActor->IsA(AAsteroid::StaticClass()) && HitComponent->IsA(UStaticMeshComponent::StaticClass()))
		{
			AAsteroid* Asteroid = Cast<AAsteroid>(OtherActor);
			if (Asteroid)
			{
				FDamageEvent DamageEvent;
				Asteroid->TakeDamage(GetImpactDamage(), DamageEvent, nullptr, this);
			}
		}

		if (OtherActor && (OtherActor != this) && OtherComponent && OtherComponent->IsSimulatingPhysics())
		{
			OtherComponent->AddImpulseAtLocation(GetVelocity() * HitImpulseStrength, GetActorLocation());
		}
		Destroy();
		ConditionalBeginDestroy();
	}
	
}


void AAsteroidsProjectile::OnComponentHit(const FHitResult& ImpactResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Projectile Stopped"))
	RadialForceComponent->FireImpulse();
	Destroy();
	ConditionalBeginDestroy();

}
