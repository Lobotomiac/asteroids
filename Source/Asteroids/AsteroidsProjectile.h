// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AsteroidsProjectile.generated.h"


class UProjectileMovementComponent;
class UStaticMeshComponent;
class URadialForceComponent;


UCLASS(config=Game)
class AAsteroidsProjectile : public AActor
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

	/** Sphere collision component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Projectile, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* ProjectileMesh = nullptr;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovement = nullptr;

	UPROPERTY(VisibleAnywhere)
	URadialForceComponent* RadialForceComponent = nullptr;

	UPROPERTY(VisibleAnywhere)
	TSubclassOf<UDamageType> DamageType;

	// Impact marker
	bool bHasHit = false;

public:
	AAsteroidsProjectile();

	UFUNCTION()
	void OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit);

	UFUNCTION()
	void OnComponentHit(const FHitResult& ImpactResult);
	/** Returns ProjectileMesh subobject **/
	FORCEINLINE UStaticMeshComponent* GetProjectileMesh() const { return ProjectileMesh; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	UPROPERTY(EditAnywhere)
	float SphereRadius = 13.0f;

	UPROPERTY(EditAnywhere)
	float ImpactDamage;

	UPROPERTY(EditAnywhere)
	float HitImpulseStrength;

	UFUNCTION()
	float GetImpactDamage() const { return ImpactDamage; }

	UFUNCTION()
	bool GetHasHit() const { return bHasHit; }

};

