// Fill out your copyright notice in the Description page of Project Settings.

#include "Asteroid.h"
#include "AsteroidsProjectile.h"
#include "Player/Earth.h"
#include "AsteroidsGameMode.h"
#include "Player/PlayerPawn.h"
#include "MapBorderVolume.h"

#include "Components/SphereComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/GameMode.h"
#include "EngineUtils.h"


// Sets default values
AAsteroid::AAsteroid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Destructible Component"));
	StaticMeshComponent->SetupAttachment(RootComponent);
	StaticMeshComponent->SetSimulatePhysics(false);
	StaticMeshComponent->bGenerateOverlapEvents = true;
	StaticMeshComponent->SetRelativeScale3D(FVector(3.84f, 3.84f, 3.84f));
	StaticMeshComponent->SetNotifyRigidBodyCollision(true);
	StaticMeshComponent->bApplyImpulseOnDamage = true;
	StaticMeshComponent->OnComponentHit.AddDynamic(this, &AAsteroid::OnHit);
	StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AAsteroid::OnOverlapBegin);
	RootComponent = StaticMeshComponent;

	// Damage System setup
	bCanBeDamaged = true;
	bIsDamagedByPlayer = false;
	bIsDestroyed = false;
	MaxHitPoints = 75.0f;
	CurrentHitPoints = MaxHitPoints;
	ImpactDamage = 20.0f;

	// Movement Setup
	MoveSpeed = 100;
	ToleranceValue = 1.1f;
	bShouldRotate = false;

	// Impact
	HitImpulseStrength = 550.0f;
}


// Called when the game starts or when spawned
void AAsteroid::BeginPlay()
{
	Super::BeginPlay();
	
	AlignAsteroidBodyWithPlayersBody();
	SetAsteroidDirection();
}


// Called every frame
void AAsteroid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateAsteroidLocation(DeltaTime);
}


// Returns the damage it would cause to other objects by Hitting
float AAsteroid::GetImpactDamage() const
{
	return ImpactDamage;
}

// When we enter a MapBorder Volume
void AAsteroid::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		UE_LOG(LogTemp, Warning, TEXT("Overlapped!"))

		if (OtherActor->IsA(AMapBorderVolume::StaticClass()))
		{
			UE_LOG(LogTemp, Warning, TEXT("Overlapped: %s"), *OtherActor->GetName())

			if (OverlappedAlready != "NoOverlapYet" &&  OverlappedAlready != OtherActor->GetName())
			{
				Destroy();
				ConditionalBeginDestroy();
			}
			OverlappedAlready = OtherActor->GetName();
		}
	}
}


// Triggering when hit by or hit something
void AAsteroid::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	if (OtherActor != this)
	{
		if (OtherActor->IsA(AAsteroid::StaticClass()))
		{
			Destroy();
			ConditionalBeginDestroy();
		}
		if (OtherActor->IsA(APlayerPawn::StaticClass()))
		{
			APlayerPawn* PlayerPawn = Cast<APlayerPawn>(OtherActor);
			if (PlayerPawn)
			{
				FDamageEvent DamageEvent;
				PlayerPawn->TakeDamage(GetImpactDamage(), DamageEvent, GetWorld()->GetFirstPlayerController(), this);
			}
			Destroy();
			ConditionalBeginDestroy();
			UE_LOG(LogTemp, Warning, TEXT("HitPlayer"))
		}

		if (OtherComponent->IsSimulatingPhysics())
		{
			OtherComponent->AddImpulseAtLocation((HitImpulseStrength * GetActorForwardVector() * GetWorld()->GetDeltaSeconds()), GetActorLocation());
		}
	}
}

float AAsteroid::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor * DamageCauser)
{
	if (DamageCauser->IsA(AAsteroidsProjectile::StaticClass()) || DamageCauser->IsA(APlayerPawn::StaticClass()))
	{
		bIsDamagedByPlayer = true;
	}
	UpdateCurrentHitPoints(DamageAmount);
	return 0.0f;
}

//TODO make HP float over its body perhaps?

// Update HitPoints after collision
void AAsteroid::UpdateCurrentHitPoints(float IncomingDamage)
{
	CurrentHitPoints -= IncomingDamage;

	if (CurrentHitPoints > 0.0f)
	{
		bIsDamagedByPlayer = false;
		ReceiveTakeDamage();
	}
	else
	{
		if (GetWorld() && bIsDamagedByPlayer && !bIsDestroyed)
		{
			auto GameModeBase = UGameplayStatics::GetGameMode(GetWorld());
			AAsteroidsGameMode* AsteroidsGameMode = Cast<AAsteroidsGameMode>(GameModeBase);
			AsteroidsGameMode->PlayerScoreUpdate(PointsForDestroying);
		}

		SetActorTickEnabled(false);
		bIsDestroyed = true;

		Destroy();
		ConditionalBeginDestroy();
	}
}


// Moves and rotates the Asteroid 
void AAsteroid::UpdateAsteroidLocation(float DeltaTime)
{
	// Update Body Location
	CurrentPosition = GetActorLocation();
	FVector NewLocation = CurrentPosition + Direction * DeltaTime * MoveSpeed;
	SetActorLocation(NewLocation, true, 0, ETeleportType::None);
}


// Aligning Asteroid to the level of the PlayerPawn
void AAsteroid::AlignAsteroidBodyWithPlayersBody()
{
		PlayerPosition = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
		CurrentPosition = GetActorLocation();
		CurrentPosition.Z = PlayerPosition.Z;
		SetActorLocation(CurrentPosition);
}

// Sets the Asteroid moving in the Earths direction
void AAsteroid::SetAsteroidDirection()
{
	// A small direction tolerance to create a more natural trajectory for the asteroid
	float DirectionTolerance = FMath::RandRange(1.0f, ToleranceValue);
	
	for (TActorIterator<AEarth> Iterator(GetWorld()); Iterator; ++Iterator)
	{	
		if (Iterator)
		{
			// Creating the target location slightly off center to make the asteroid miss sometimes
			auto ActorLocation = Iterator->GetActorLocation();
			ActorLocation.X *= ToleranceValue;
			ActorLocation.Y *= ToleranceValue;
			Direction = (ActorLocation - CurrentPosition).GetSafeNormal();
		}
	}
}

