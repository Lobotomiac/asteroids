// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Asteroid.generated.h"

class UStaticMeshComponent;
class AAsteroidsGameMode;



UCLASS()
class ASTEROIDS_API AAsteroid : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AAsteroid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void AlignAsteroidBodyWithPlayersBody();//	sets the root altitude to match the players
	void SetAsteroidDirection();			//	sets the direction the asteroid will move in
	void UpdateAsteroidLocation(float DeltaTime);


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	UStaticMeshComponent* StaticMeshComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	float MaxHitPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	float CurrentHitPoints;

	// To make sure we know the source of the killing blow 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	bool bIsDamagedByPlayer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	bool bIsDestroyed;
	
	UPROPERTY(VisibleAnywhere, Category = "Score System")
	int32 PointsForDestroying = 100;

	UPROPERTY(EditAnywhere, Category = "Damage")
	float ImpactDamage;

	UFUNCTION()
	void UpdateCurrentHitPoints(float IncomingDamage);
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Should the component rotate? ( the component isn't doing a sweep if rotating )
	bool bShouldRotate;

	// The MapBorderVolume
	FString OverlappedAlready = "NoOverlapYet";

public:	
	// Makes the speed of movement 
	UPROPERTY(EditAnywhere, Category = "Movement")
	float MoveSpeed;

	UPROPERTY(EditAnywhere, Category = "Impact")
	float HitImpulseStrength;

	FVector PlayerPosition;
	FVector CurrentPosition;
	FVector Direction;

	// Keep to 
	UPROPERTY(EditAnywhere, Category = "Movement")
	float ToleranceValue;

	float GetImpactDamage() const;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit);

	UFUNCTION()
	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController * EventInstigator, AActor * DamageCauser);

	// Will play a sound
	UFUNCTION(BlueprintImplementableEvent, Category = "Sound")
	void ReceiveTakeDamage();

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHitPoints() const { return CurrentHitPoints; }
};
