// Fill out your copyright notice in the Description page of Project Settings.

#include "ChargerShip.h"
#include "AsteroidsProjectile.h"
#include "Player/PlayerPawn.h"
#include "Player/Earth.h"
#include "Asteroid.h"
#include "AsteroidsGameMode.h"
#include "Player/HealthPack.h"

#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "DrawDebugHelpers.h"


// TODO Add a material color change when getting closer to explode maybe?
// TODO Write a description 
/*
	
Functionality:

FindPlayer && ChargeTowardsPlayer,  Homing Charge?  // => Handling it with the default ProjectileMovementComponent
After ExplosionRadius reaches player,
	if player doesn't escape Explosive radius for in TIME, 
		Explode
*/

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue,text)

// Sets default values
AChargerShip::AChargerShip()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	// Setting up the MeshComponent
	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyMesh"));
	if (BodyMesh)
	{
		RootComponent = BodyMesh;
		BodyMesh->bGenerateOverlapEvents = false;
		BodyMesh->SetSimulatePhysics(false);
		BodyMesh->SetNotifyRigidBodyCollision(true);
		BodyMesh->SetRelativeScale3D(FVector(0.8f, 0.8f, 0.8f));
		BodyMesh->OnComponentHit.AddDynamic(this, &AChargerShip::OnHit);
	}	else	{	print("ChargerShip BodyMesh failed");	}

	// Setting up the SphereComponent 
	ExplosionCapsule = CreateDefaultSubobject<USphereComponent>(TEXT("ExplosionCapsule"));
	if (ExplosionCapsule)
	{
		ExplosionCapsule->SetupAttachment(RootComponent);
		ExplosionCapsule->SetSimulatePhysics(false);
		ExplosionCapsule->bGenerateOverlapEvents = true;
		ExplosionCapsule->InitSphereRadius(SphereRadius / 0.8);
		ExplosionCapsule->bHiddenInGame = false;
		ExplosionCapsule->OnComponentBeginOverlap.AddDynamic(this, &AChargerShip::OnOverlapBegin);
		ExplosionCapsule->OnComponentEndOverlap.AddDynamic(this, &AChargerShip::OnOverlapEnd);
	}	else	{	print("ChargerShip ExplosionCapsule failed");	}

	// Setting up the MovementComponent
	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComponent"));
	if (MovementComponent)
	{
		MovementComponent->UpdatedComponent = BodyMesh;
		MovementComponent->InitialSpeed = 100.f;
		MovementComponent->MaxSpeed = 1500.f;
		MovementComponent->HomingAccelerationMagnitude = 2000.0f;
		MovementComponent->bRotationFollowsVelocity = true;
		MovementComponent->bShouldBounce = true;
		MovementComponent->Bounciness = 0.0f;
		MovementComponent->Friction = 0.0f;
		MovementComponent->BounceVelocityStopSimulatingThreshold = 0.0f;
		MovementComponent->ProjectileGravityScale = 0.f; // No gravity
		MovementComponent->bIsHomingProjectile = true;
	}	else { print("ChargerShip MovementComponent failed"); }
	
	// Setting up the Physical force emiting component (pushes stuff around when fired)
	RadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	if (RadialForceComponent)
	{
		RadialForceComponent->SetupAttachment(RootComponent);
		RadialForceComponent->Radius = SphereRadius;
		RadialForceComponent->ImpulseStrength = 1000.0f;
		RadialForceComponent->bImpulseVelChange = true;
	}	else { print("ChargerShip RadialForceComponent failed"); }
	

	MaxHitPoints = 100.0f;
	CurrentHitPoints = MaxHitPoints;

	bCanBeDamaged = true;
	bIsDamagedByPlayer = false;
	TimeToExplode = 2.0f;
	ExplosionStrength = 75.0f;
	ExplosionTriggerFailsafe = 0.5f;

	bFirstOverlap = false;
	bIsOverlappingPlayer = false;
	ProximitySoundIntervalBase = 2.0f;
	ProximitySoundInterval = ProximitySoundIntervalBase;
	ProximitySoundVolumeBase = 0.4f;
	ProximitySoundVolume = ProximitySoundVolumeBase;

	IntervalIncrement = 0.05f;
	VolumeIncrement = 0.03;
}


// Called when the game starts or when spawned
void AChargerShip::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();
	ExplosionCapsule->SetCollisionProfileName("Trigger");

	// Lock on to the Player's position
	SetTarget();
	
	//	getting the player location, making sure ChargerShip is aligned
	//	to the same height to begin with  and remain on the same height throughout the whole game
	AlignWithPlayer();

	World->GetTimerManager().SetTimer(BeepingTimerHandle, this, &AChargerShip::EmitSound, ProximitySoundInterval);
}


// going through what overlaps are happening
void AChargerShip::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherActor->IsA(APlayerPawn::StaticClass()) && OtherComp->IsA(UStaticMeshComponent::StaticClass()))
	{
		// Set Timer to explode if overlap doesn't end
		World->GetTimerManager().SetTimer(ExplosionTimeHandle, this, &AChargerShip::Explode, TimeToExplode);
		
		// Stop the regular sound timer
		World->GetTimerManager().ClearTimer(BeepingTimerHandle);
	
		bIsOverlappingPlayer = true;
		bFirstOverlap = true;
		EmitSound();
	}
}


void AChargerShip::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (!OtherActor)
	{
		return;
	}

	if (OtherActor->IsA(APlayerPawn::StaticClass()))
	{
		if (OverlappedComp->IsA(USphereComponent::StaticClass()))
		{
			World->GetTimerManager().ClearTimer(ExplosionTimeHandle);

			/* Reset the sound emission settings to base values 
			(so we don't do it every time in the EmitSound function every time it gets called)*/
			bIsOverlappingPlayer = false;
			ProximitySoundInterval = ProximitySoundIntervalBase;
			ProximitySoundVolume = ProximitySoundVolumeBase; 
		}
	}
}


void AChargerShip::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!OtherActor || (OtherActor == this))
	{	
		return;
	}
	// Choosing impact Consequence 
	if (OtherActor->IsA(APlayerPawn::StaticClass()) && !bHasExploded)
	{
		bIsDamagedByPlayer = true;
		Explode();
	}
	else if (OtherActor->IsA(AAsteroidsProjectile::StaticClass()) && OtherComponent->IsA(UStaticMeshComponent::StaticClass()))
	{
		bIsDamagedByPlayer = true;
		AAsteroidsProjectile* Projectile = Cast<AAsteroidsProjectile>(OtherActor);
		if (Projectile)
		{
			FDamageEvent DamageEvent;
			TakeDamage(Projectile->GetImpactDamage(), DamageEvent, nullptr, OtherActor);
		}
	}
	else if (OtherActor->IsA(AEarth::StaticClass()) && !bHasExploded)
	{
		// TODO perhaps change the mechanism to check distance from player OnOverlap?
		// How much the ship travels per frame
		float LastTimeOnScreen = this->GetLastRenderTime();
		float CurrentTime = World->GetTimeSeconds();

		if (ExplosionTriggerFailsafe + LastTimeOnScreen > CurrentTime)
		{
			Explode();
		}
		
	}
	//else if (OtherActor->IsA(AAsteroid::StaticClass()) && !bHasExploded)
	//{
	//	Explode();
	//}
	//else if (OtherActor->IsA(AChargerShip::StaticClass()) && !bHasExploded)
	//{
	//	// TODO perhaps make them bounce of and delay a small random amount before speeding up?
	//	Explode();
	//}
	else
	{
		Explode();
	}
}

// Handles the damage
float AChargerShip::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	if (!bHasExploded)
	{
		UpdateCurrentHitPoints(DamageAmount);
	}
	return DamageAmount;
}

void AChargerShip::EmitSound()
{
	if (bFirstOverlap)
	{
		ProximitySoundInterval /= ProximitySoundInterval * 2;
		bFirstOverlap = false;
	}
	else if (bIsOverlappingPlayer)
	{
		// Making sure the intervals are subtracted at a sort of a pleasant rate
		ProximitySoundInterval -= ProximitySoundInterval / IntervalIncrement;
		ProximitySoundInterval = FMath::Clamp(ProximitySoundInterval, 0.1f, ProximitySoundIntervalBase);
		ProximitySoundVolume += VolumeIncrement;
		ProximitySoundVolume = FMath::Clamp(ProximitySoundVolume, ProximitySoundVolumeBase, 1.0f);
	}
	else
	{
		// resetting original values
		ProximitySoundVolume = ProximitySoundVolumeBase;
		ProximitySoundInterval = ProximitySoundIntervalBase;
	}
	ReceiveEmitSound();
	World->GetTimerManager().SetTimer(BeepingTimerHandle, this, &AChargerShip::EmitSound, ProximitySoundInterval);
}

// Update Hit Points 
void AChargerShip::UpdateCurrentHitPoints(float Damage)
{
	CurrentHitPoints -= Damage;
	if (CurrentHitPoints > 0.0f)
	{
		bIsDamagedByPlayer = false;
		ReceiveTakeDamage();
	}
	else
	{
		if (World && bIsDamagedByPlayer && !bHasExploded)
		{
			auto GameModeBase = UGameplayStatics::GetGameMode(World);
			AAsteroidsGameMode* AsteroidsGameMode = Cast<AAsteroidsGameMode>(GameModeBase);
			AsteroidsGameMode->PlayerScoreUpdate(PointsForDestroying);
		}
		Explode();
	}
}

// Boom
void AChargerShip::Explode()
{
	if (!bHasExploded)
	{
		bHasExploded = true;
		TSubclassOf<UDamageType> DamageType;
		bool bDamageApplied; 
		bDamageApplied = UGameplayStatics::ApplyRadialDamage
		(	this,
			ExplosionStrength, 
			GetActorLocation(),
			SphereRadius, 
			DamageType, 
			TArray<AActor *>(), 
			this, 
			nullptr, 
			true, 
			ECollisionChannel::ECC_Visibility
		);
		RadialForceComponent->FireImpulse();
	}

	if (bIsDamagedByPlayer)
	{
		int32 RandomDropValue = FMath::RandRange(0, 15);
		int32 RandomDropChance = RandomDropValue % 3;
		if (RandomDropChance == 0)
		{
			// Spawn HealthPack
			FTransform SpawnTransform = GetActorTransform();
			FActorSpawnParameters SpawnParams;
			if (World)
			{
				World->SpawnActor<AHealthPack>(HealthPackBlueprint, SpawnTransform, SpawnParams);
			}
		}

	}
	Destroy();
	ConditionalBeginDestroy();
}


void AChargerShip::AlignWithPlayer()
{
	FVector PlayerLocation = World->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	FVector CurrentLocation = GetActorLocation();
	CurrentLocation.Z = PlayerLocation.Z;
	SetActorLocation(CurrentLocation);
}


void AChargerShip::SetTarget()
{
	if (!World) {	return;	}
	// Declaring a weak pointer which turns out to be more of a pain than I thought 
	TWeakObjectPtr <USceneComponent> HomingTarget = World->GetFirstPlayerController()->GetPawn()->GetRootComponent();
	if (HomingTarget.IsValid())
	{
		MovementComponent->HomingTargetComponent = HomingTarget;
	}
	else
	{ 
		UE_LOG(LogTemp, Warning, TEXT("No HomingTarget"))
	}
}
