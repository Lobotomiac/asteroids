// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ChargerShip.generated.h"

class UStaticMeshComponent;
class UProjectileMovementComponent;
class USphereComponent;
class URadialForceComponent;
class AHealthPack;

UCLASS()
class ASTEROIDS_API AChargerShip : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BodyMesh = nullptr;

	UPROPERTY(VisibleAnywhere)
	UProjectileMovementComponent* MovementComponent = nullptr;

	UPROPERTY(VisibleAnywhere)
	USphereComponent* ExplosionCapsule = nullptr;

	UPROPERTY(VisibleAnywhere)
	URadialForceComponent* RadialForceComponent = nullptr;

public:
	// Sets default values for this actor's properties
	AChargerShip();	

	UFUNCTION()
	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController * EventInstigator, AActor * DamageCauser);

	UFUNCTION(BlueprintImplementableEvent, Category = "Sound")
	void ReceiveTakeDamage();

	// Radius of the ContactCapsule
	UPROPERTY(EditAnywhere, Category = "Explosion")
	float SphereRadius =  500.0f;

	UPROPERTY(EditAnywhere, Category = "Explosion")
	float TimeToExplode;

	UPROPERTY(EditAnywhere, Category = "Explosion")
	float ExplosionStrength;

	// Event for emitting sound in BPs
	UFUNCTION(BlueprintImplementableEvent, Category = "Explosion")
	void ReceiveEmitSound();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Aligns the ship body with the players
	void AlignWithPlayer();	

	// Sets the player as the homing target
	void SetTarget();

	// Destroys itself on collision or 0 HitPoints
	void Explode();

	// Called when something STARTS overlapping the actor components
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	// Called when something STOPS overlapping the actor components
	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// To continue being condescending this is called when the ChargerShip MESH gets hit by an object
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	// Handle to control the time for charging the player again
	FTimerHandle ExplosionTimeHandle;

	// How far away from player will we allow for the ChargerShip to explode
	UPROPERTY(EditAnywhere, Category = "Explosion")
	float ExplosionTriggerFailsafe;

	// Proximity sound mechanic
	FTimerHandle BeepingTimerHandle;

	/* HitPoints system */
	UPROPERTY(VisibleAnywhere, Category = "HitPoints")
	float MaxHitPoints;

	UPROPERTY(VisibleAnywhere, Category = "HitPoints")
	float CurrentHitPoints;

	/* Sound mechanic setup */
	// How fast will the sound be repeating
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sound", meta = (ClampMin = 0.1f))
	float ProximitySoundInterval;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sound")
	float ProximitySoundIntervalBase;

	// How loud the sound will be
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sound", meta = (ClampMin = 0.01f, ClampMax = 1.0f))
	float ProximitySoundVolume;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sound")
	float ProximitySoundVolumeBase;
	// How quickly does the sound emission speed up after getting close to player
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sound", meta = (ClampMin = 1.2f, ClampMax = 5.0f))
	float IntervalIncrement;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sound", meta = (ClampMin = 0.01f, ClampMax = 0.5f))
	float VolumeIncrement;

	bool bFirstOverlap;
	bool bIsOverlappingPlayer;
	
	UFUNCTION()
	void EmitSound();

	UFUNCTION()
	void UpdateCurrentHitPoints(float Damage);

	bool bIsDamagedByPlayer = false;

	bool bHasExploded = false;

	int32 PointsForDestroying = 250;

	UPROPERTY()
	UWorld* World = nullptr;
	
private:
	
	UPROPERTY(EditDefaultsOnly, Category = "Loot")
	TSubclassOf<AHealthPack> HealthPackBlueprint;
};
