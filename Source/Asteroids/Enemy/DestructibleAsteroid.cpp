// Fill out your copyright notice in the Description page of Project Settings.

#include "DestructibleAsteroid.h"
#include "AsteroidsProjectile.h"
#include "AsteroidsGameMode.h"
#include "Player/PlayerPawn.h"

#include "GameFramework/GameMode.h"
#include "Components/PrimitiveComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DestructibleComponent.h"

//TODO create a random location spawning mechanism of sorts


// Sets default values
ADestructibleAsteroid::ADestructibleAsteroid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BodyInstance.bGenerateWakeEvents = true;
	BodyInstance.bSimulatePhysics = true;
	BodyInstance.bEnableGravity = false;
	// TODO change scale of the body
	SetActorRelativeScale3D(BodyScale);
	GetDestructibleComponent()->OnComponentHit.AddDynamic(this, &ADestructibleAsteroid::OnHit);
	RootComponent = GetDestructibleComponent();
	UpdateOverlaps(true);
	
	// Damage System setup
	bCanBeDamaged = true;
	bIsDamagedByPlayer = false;
	bIsDestroyed = false;
	MaxHitPoints = 75.0f;
	CurrentHitPoints = MaxHitPoints;
	ImpactDamage = 20.0f; // damage to suffer on Impact with objects
	
	MoveSpeed = FMath::RandRange(50.f, 200.f);
}


// Called when the game starts or when spawned
void ADestructibleAsteroid::BeginPlay()
{
	Super::BeginPlay();

	GetDestructibleComponent()->SetNotifyRigidBodyCollision(true);

	// Seeding the rand value to make it different each time we call it & setting a random rotation velocity 
	// (limited to 1 otherwise rotates to fast)
	FMath::SRand();
	Pitch = FMath::RandRange(-1.0f, 1.0f);
	Yaw = FMath::RandRange(-1.0f, 1.0f);
	Roll = FMath::RandRange(-1.0f, 1.0f);

	// Aligning Asteroid to the level of the PlayerPawn
	PlayerPosition = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	CurrentPosition = GetActorLocation();
	CurrentPosition.Z = PlayerPosition.Z;
	SetActorLocation(CurrentPosition);
	
	Direction = (PlayerPosition - CurrentPosition).GetUnsafeNormal();	// Sets the Asteroid moving in the players direction
}


// Called every frame
void ADestructibleAsteroid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateRotationAndLocation(DeltaTime);
}


float ADestructibleAsteroid::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor * DamageCauser)
{
	if (DamageCauser->IsA(AAsteroidsProjectile::StaticClass()) || DamageCauser->IsA(APlayerPawn::StaticClass()))
	{
		bIsDamagedByPlayer = true;
		UE_LOG(LogTemp, Warning, TEXT("%s : Taking %.2f damage!"), *GetName(), DamageAmount)
	}
	UpdateCurrentHitPoints(DamageAmount);
	return 0.0f;
}

void ADestructibleAsteroid::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("%s : Damaged by player"), *GetName())

	//OnActorFracture
}

//TODO make HP float over its body perhaps?

// Update HitPoints after collision
void ADestructibleAsteroid::UpdateCurrentHitPoints(float IncomingDamage)
{
	CurrentHitPoints -= IncomingDamage;
	if (CurrentHitPoints > 0.0f)
	{
		bIsDamagedByPlayer = false;
		UE_LOG(LogTemp, Warning, TEXT("%s : %.2f HitPoints remaining!"), *GetName(), CurrentHitPoints)
		UE_LOG(LogTemp, Warning, TEXT("%s : Physics : %s"), *GetName(), GetDestructibleComponent()->IsSimulatingPhysics() ? TEXT("True") : TEXT("False"))
		UE_LOG(LogTemp, Warning, TEXT("%s : Is Awake?  %s"), *GetName(), BodyInstance.IsInstanceAwake() ? TEXT("True") : TEXT("False"))
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ADestructibleAsteroid: DESTROYED!!"), CurrentHitPoints)
			if (GetWorld() && bIsDamagedByPlayer && !bIsDestroyed)
			{
				auto GameModeBase = UGameplayStatics::GetGameMode(GetWorld());
				AAsteroidsGameMode* AsteroidsGameMode = Cast<AAsteroidsGameMode>(GameModeBase);
				AsteroidsGameMode->PlayerScoreUpdate(PointsForDestroying);
			}
		SetActorTickEnabled(false);
		bIsDestroyed = true;
		UE_LOG(LogTemp, Warning, TEXT("%s : DestructibleMesh Physics is %s"), *GetName(), GetDestructibleComponent()->IsSimulatingPhysics()? TEXT("True") : TEXT("False"))
		UE_LOG(LogTemp, Warning, TEXT("%s : DestructibleMesh is %s"), *GetName(), BodyInstance.IsInstanceAwake() ? TEXT("True") : TEXT("False"))

		//Destroy();
	}
}


// Moves and rotates the Asteroid 
void ADestructibleAsteroid::UpdateRotationAndLocation(float DeltaTime)
{
	FQuat QuatBodyRotation = FQuat(FRotator(Pitch, Yaw, Roll));
	AddActorLocalRotation(QuatBodyRotation, true, nullptr, ETeleportType::None);

	CurrentPosition = GetActorLocation();
	FVector NewLocation = CurrentPosition + Direction * DeltaTime * MoveSpeed;
	SetActorLocation(NewLocation);
}





