// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DestructibleActor.h"
#include "DestructibleAsteroid.generated.h"

/**
 * 
 */

class UDestructibleComponent;

UCLASS()
class ASTEROIDS_API ADestructibleAsteroid : public ADestructibleActor
{
	GENERATED_BODY()
	
	
public:
	// Sets default values for this actor's properties
	ADestructibleAsteroid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void UpdateRotationAndLocation(float DeltaTime);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	float MaxHitPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	float CurrentHitPoints;

	// To make sure we know the source of the killing blow 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	bool bIsDamagedByPlayer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	bool bIsDestroyed;

	UPROPERTY(VisibleAnywhere, Category = "Score System")
	int32 PointsForDestroying = 100;

	FBodyInstance BodyInstance;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	

	UPROPERTY(EditAnywhere)
	float ImpactDamage;

	UPROPERTY(EditAnywhere)
	float SphereRadius = 50.0f;

	UFUNCTION()
	void UpdateCurrentHitPoints(float IncomingDamage);

	UFUNCTION()
	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController * EventInstigator, AActor * DamageCauser);

	UFUNCTION()
	void OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit);

private:
	/// Movement
	// Body rotation
	float Pitch;
	float Yaw;
	float Roll;

	UPROPERTY(EditInstanceOnly, Category = "Movement")
		float MoveSpeed;	// Makes the speed of movement 

	FVector PlayerPosition;
	FVector CurrentPosition;
	FVector Direction;

	FVector BodyScale = FVector(4.0f, 4.0f, 4.0f);
};
