// Fill out your copyright notice in the Description page of Project Settings.

#include "SpawningVolume.h"
#include "Enemy/Asteroid.h"
#include "Enemy/ChargerShip.h"
#include "Player/Earth.h"

#include "EngineUtils.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"

// TODO clean this up, fix DRY cases


// Sets default values
ASpawningVolume::ASpawningVolume()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnBoxComponent"));
	SpawnBoxComponent->bGenerateOverlapEvents = false;
	SpawnBoxComponent->SetRelativeScale3D(FVector(160.0f, 160.0f, 20.0f));
	RootComponent = SpawnBoxComponent;

	SpawnDistanceFromEarthMIN = 2000.0f;
	AsteroidSpawnTime = 1.0f;
	ChargerShipSpawnTime = 2.01f;
}

// Called when the game starts or when spawned
void ASpawningVolume::BeginPlay()
{
	Super::BeginPlay();

	
	World = GetWorld();

	World->GetTimerManager().SetTimer(AsteroidSpawnTimeHandle, this, &ASpawningVolume::SpawnAsteroid, AsteroidSpawnTime);
	World->GetTimerManager().SetTimer(ChargerShipSpawnTimerHandle, this, &ASpawningVolume::SpawnChargerShip, ChargerShipSpawnTime);
	
}

void ASpawningVolume::SpawnAsteroid()
{	
	World->SpawnActor<AAsteroid>(Asteroid, CalculateSpawnLocation(), GetActorRotation());
	World->GetTimerManager().SetTimer(AsteroidSpawnTimeHandle, this, &ASpawningVolume::SpawnAsteroid, AsteroidSpawnTime);
}


void ASpawningVolume::SpawnChargerShip()
{
	World->SpawnActor<AChargerShip>(ChargerShip, CalculateSpawnLocation(), GetActorRotation());
	World->GetTimerManager().SetTimer(ChargerShipSpawnTimerHandle, this, &ASpawningVolume::SpawnChargerShip, ChargerShipSpawnTime);
}


FVector ASpawningVolume::CalculateSpawnLocation()
{
	FVector EarthPosition;

	for (TActorIterator<AEarth> Iterator(World); Iterator; ++Iterator)
	{
		if (Iterator)
		{
			EarthPosition = Iterator->GetActorLocation(); // Finding the Earth and getting its location
		}
	}

	// Get the extent of each side of the spawning volume
	FVector ScaledBoxExtent = SpawnBoxComponent->GetScaledBoxExtent(); // size of spawning Volume

	// Making sure the Spawn location is inside the SpawningVolume, and that the Height is aligned with the player to make sure there is no deviation in height distance from player
	FVector SpawnLocation = FVector(FMath::RandRange(-ScaledBoxExtent.X, ScaledBoxExtent.X), FMath::RandRange(-ScaledBoxExtent.Y, ScaledBoxExtent.Y), ScaledBoxExtent.Z) + GetActorLocation();
	SpawnLocation.Z = EarthPosition.Z;

	float SpawnDistanceFromEarth = (EarthPosition - SpawnLocation).Size();
	//	Make sure that the Asteroid spawns far enough from the player
	while ((SpawnDistanceFromEarth <= SpawnDistanceFromEarthMIN) && (-SpawnDistanceFromEarth >= -SpawnDistanceFromEarthMIN))
	{
		SpawnLocation.X = FMath::RandRange(-ScaledBoxExtent.X, ScaledBoxExtent.X);
		SpawnLocation.Y = FMath::RandRange(-ScaledBoxExtent.Y, ScaledBoxExtent.Y);
		SpawnLocation += GetActorLocation();
		SpawnLocation.Z = EarthPosition.Z;
		SpawnDistanceFromEarth = (EarthPosition - SpawnLocation).Size();
	}
	return SpawnLocation;
}