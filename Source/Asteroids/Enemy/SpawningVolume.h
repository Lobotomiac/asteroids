// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawningVolume.generated.h"


/* A volume to spawn enemies in, and to prevent spawning to close to the player pawn */



class AAsteroid;
class AChargerShip;
class UBoxComponent;


UCLASS()
class ASTEROIDS_API ASpawningVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawningVolume();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	// TODO create an array of both enemies to help with memory leaks
	UPROPERTY(EditAnywhere, Category = "Spawning")
	TSubclassOf<AAsteroid> Asteroid;

	UPROPERTY(EditAnywhere, Category = "Spawning")
	TSubclassOf<AChargerShip> ChargerShip;

	FTimerHandle AsteroidSpawnTimeHandle;
	FTimerHandle ChargerShipSpawnTimerHandle;

	UPROPERTY(EditAnywhere, Category = "Spawning")
	float AsteroidSpawnTime;
	
	UPROPERTY(EditAnywhere, Category = "Spawning")
	float ChargerShipSpawnTime;

	UFUNCTION()
	void SpawnAsteroid();

	UFUNCTION()
	void SpawnChargerShip();

	// Finds a good position to spawn objects in, making sure it is far enough from the player
	FVector CalculateSpawnLocation();
	
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* SpawnBoxComponent = nullptr;


	// World handle
	UPROPERTY()
	UWorld* World = nullptr;

	// Minimal distance from player to spawn
	UPROPERTY(EditAnywhere, Category = "Spawning")
	float SpawnDistanceFromEarthMIN;
	
};
