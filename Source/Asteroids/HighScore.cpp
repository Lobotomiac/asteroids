// Fill out your copyright notice in the Description page of Project Settings.

#include "HighScore.h"




UHighScore::UHighScore()
{
	HighScoreListLength = 10;

}


// Updating and sorting the array of high scores 
void UHighScore::UpdateHighScoreList(int32 NewScoreEntry)
{
	// Add current score to HighScore array
	HighScoreList.Add(NewScoreEntry);
	// Sort Array in a descending order
	HighScoreList.Sort([](const int32& A, const int32& B) {	return A > B; });

	// If our list is longer than we decide on how many scores we'd want to show, reduce it to the desired size
	if (HighScoreList.Num() > HighScoreListLength)
	{
		HighScoreList.SetNum(HighScoreListLength);
	}
}
