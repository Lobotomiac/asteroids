// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "HighScore.generated.h"

/**
 * 
 */
UCLASS()
class ASTEROIDS_API UHighScore : public USaveGame
{
	GENERATED_BODY()
	
public:
	UHighScore();

	TArray<int32> GetHighScoreList() const {return HighScoreList;};	

	void UpdateHighScoreList(int32 NewScoreEntry); 


private:	
	// List of High Scores at the end of games
	UPROPERTY(VisibleAnywhere, Category = "Score")
	TArray<int32> HighScoreList;
	
	UPROPERTY(EditDefaultsOnly, Category = "Score")
	int32 HighScoreListLength;
};
