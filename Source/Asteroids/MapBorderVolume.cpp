// Fill out your copyright notice in the Description page of Project Settings.

#include "MapBorderVolume.h"
#include "Player/PlayerPawn.h"

#include "DrawDebugHelpers.h"

AMapBorderVolume::AMapBorderVolume()
{
	bEntryPain = false;
	bPainCausing = true;

	bIsPlayerStillInside = false;
	// How often to damage player
	PainInterval = 1.0f;

	OnActorBeginOverlap.AddDynamic(this, &AMapBorderVolume::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AMapBorderVolume::OnOverlapEnd);
}

void AMapBorderVolume::BeginPlay()
{
	Super::BeginPlay();

	DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), FColor::Yellow, true, -1.0f, 0, 55.0f);
}


// Check if player hasn't left the volume reset the hurt timer, restart the timer if he had
void AMapBorderVolume::OnOverlapBegin(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && OtherActor != this)
	{
		if (OtherActor->IsA(APlayerPawn::StaticClass()))
		{
			// Create a pointer to the player so we know who we're supposed to hurt
			Player = Cast<APlayerPawn>(OtherActor);
			if (bIsPlayerStillInside)
			{
				HurtPlayer(); // Hopefully this is completely understood
			}
			// Marking the player is ocupying the volume 
			bIsPlayerStillInside = true;

			// Start the timer for next damage instance
			GetWorldTimerManager().SetTimer(TimerHandle_PainTimer, this, &AMapBorderVolume::HurtPlayer, PainInterval, false);
		
		}	
	}
}

void AMapBorderVolume::OnOverlapEnd(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && OtherActor != this)
	{
		if (OtherActor->IsA(APlayerPawn::StaticClass()))
		{
			bIsPlayerStillInside = false;
	
			GetWorldTimerManager().ClearAllTimersForObject(this);
		}
	}
}

void AMapBorderVolume::HurtPlayer()
{
	CausePainTo(Player);
	if (bIsPlayerStillInside)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_PainTimer, this, &AMapBorderVolume::HurtPlayer, PainInterval, false);
	}
}
