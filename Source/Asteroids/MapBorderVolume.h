// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PainCausingVolume.h"
#include "MapBorderVolume.generated.h"


class APlayerPawn;

/**
 * 
 */
UCLASS(Blueprintable)
class ASTEROIDS_API AMapBorderVolume : public APainCausingVolume
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public: 
	AMapBorderVolume();

	// Overlap Functions
	UFUNCTION()
	void OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
	void OnOverlapEnd(AActor* OverlappedActor, AActor* OtherActor);
	
	void HurtPlayer();

	// Indicator that the player is still inside the volume
	bool bIsPlayerStillInside = false;

	UPROPERTY()
	APlayerPawn* Player = nullptr;
	
};
