// Fill out your copyright notice in the Description page of Project Settings.

#include "Moon.h"
#include "Player/Earth.h"

#include "Components/StaticMeshComponent.h"
#include "EngineUtils.h"

// Sets default values
AMoon::AMoon()
{
 	// Set this actor to call Tick() every frame.  
	PrimaryActorTick.bCanEverTick = true;

	MoonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MoonMesh"));
	MoonMesh->bGenerateOverlapEvents = true;
	MoonMesh->SetNotifyRigidBodyCollision(true);
	MoonMesh->SetCollisionProfileName("IgnoreOnlyPawn");
	RootComponent = MoonMesh;

	RotationRadius = 2000.0f;
	RotationAngle = 0.0f;
	// Realistic 
	OrbitRate = 0.00313;
}

// Called when the game starts or when spawned
void AMoon::BeginPlay()
{
	Super::BeginPlay();
	
	for (AEarth* EarthIterator : TActorRange<AEarth>(GetWorld()))
	{
		Earth = EarthIterator;
	}

	// Setting the real scale of the Moon relative to the Earth
	if (Earth)
	{
		FVector EarthRelativeScale = Earth->GetActorScale3D();
		this->SetActorScale3D(EarthRelativeScale * 0.27);

		// Setting the real relative distance from earth
		float EarthRadius = Earth->GetSimpleCollisionRadius();
		RotationRadius = EarthRadius * 59.0f;
	}
	
}

// Called every frame
void AMoon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Location around which the Moon will rotate
	FVector CenterLocation = FVector(Earth->GetActorLocation());

	// Radius of the rotation
	FVector Radius = FVector(RotationRadius, 0.0f, 0.0f);

	RotationAngle -= OrbitRate;

	if (RotationAngle < -360.0f)
	{
		UE_LOG(LogTemp, Warning, TEXT("Full Orbit done in: %f"), GetWorld()->GetTimeSeconds())
		RotationAngle = 0.0f;
	}

	FVector RotationValue = Radius.RotateAngleAxis(RotationAngle, FVector(0.0f, 0.0f, 1.0f));

	CenterLocation += RotationValue;

	SetActorLocation(CenterLocation);
}

