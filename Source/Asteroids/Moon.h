// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Moon.generated.h"

class UStaticMeshComponent;
class AEarth;

UCLASS()
class ASTEROIDS_API AMoon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMoon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* MoonMesh = nullptr;

	UPROPERTY()
	AEarth* Earth = nullptr;

	// Rotation Degree
	float RotationAngle;

	UPROPERTY(EditAnywhere)
	float RotationRadius;

	UPROPERTY(EditAnywhere)
	float OrbitRate;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
		
};
