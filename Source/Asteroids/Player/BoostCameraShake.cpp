// Fill out your copyright notice in the Description page of Project Settings.

#include "BoostCameraShake.h"


UBoostCameraShake::UBoostCameraShake()
{
	OscillationDuration = 1.0f;
	OscillationBlendInTime = 0.05f;
	OscillationBlendOutTime = 0.05f;

	RotOscillation.Pitch.Amplitude = 0.05;
	RotOscillation.Pitch.Frequency = 40.0f;

	RotOscillation.Yaw.Amplitude = 0.05;
	RotOscillation.Yaw.Frequency = 55.0f;
}

