// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "BoostCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class ASTEROIDS_API UBoostCameraShake : public UCameraShake
{
	GENERATED_BODY()
	
	UBoostCameraShake();
	
	
};
