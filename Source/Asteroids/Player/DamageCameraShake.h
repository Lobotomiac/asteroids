// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "DamageCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class ASTEROIDS_API UDamageCameraShake : public UCameraShake
{
	GENERATED_BODY()
	
public:
	UDamageCameraShake();
};
