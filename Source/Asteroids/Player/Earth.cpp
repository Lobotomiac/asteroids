// Fill out your copyright notice in the Description page of Project Settings.

#include "Earth.h"
#include "Enemy/Asteroid.h"
#include "Enemy/ChargerShip.h"
#include "HighScore.h"
#include "AsteroidsGameMode.h"

#include "Components/StaticMeshComponent.h"


// Sets default values
AEarth::AEarth()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false; // TODO FIGURE OUT HOW TO TURN OF TICK COMPLETELY ( ITS STILL BEING CALLED (and not just this actor)))

	EarthMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Earth Mesh Component"));
	RootComponent = EarthMesh;
	EarthMesh->OnComponentHit.AddDynamic(this, &AEarth::OnHit);
	EarthMesh->SetNotifyRigidBodyCollision(true);
	EarthMesh->SetSimulatePhysics(false);
	EarthMesh->SetCollisionProfileName("NoCollision");
	
	// add on component Destroyed?
	ShieldMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Shield Mesh Component"));
	ShieldMesh->SetupAttachment(RootComponent);
	ShieldMesh->OnComponentHit.AddDynamic(this, &AEarth::OnHit);
	ShieldMesh->SetNotifyRigidBodyCollision(true);
	ShieldMesh->SetSimulatePhysics(false);
	
	ShieldHealthMax = 500.0f;
	EarthHealthMax = 100.0f; 
	DamageReduction = 50.0f;
	bCanBeDamaged = true;
	bHasShield = true;
}

// Called when the game starts or when spawned
void AEarth::BeginPlay()
{
	Super::BeginPlay();

	GameModeBase = GetWorld()->GetAuthGameMode();
	ThisGameMode = Cast<AAsteroidsGameMode>(GameModeBase);
	
	ShieldHealthCurrent = ShieldHealthMax;
	EarthHealthCurrent = EarthHealthMax;

	SetActorTickEnabled(false);
	DamageReduction = FMath::Clamp(DamageReduction, 0.0f, 100.0f);	 // Setting the "Armor" value for reducing radial damage

	AlignBodyWithPlayersBody();
}


void AEarth::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	if (OtherActor && OtherActor != this)
	{
		if (OtherActor->IsA(AAsteroid::StaticClass()))
		{
			AAsteroid* Asteroid = Cast<AAsteroid>(OtherActor);
			FDamageEvent DamageEvent;
			TakeDamage(Asteroid->GetImpactDamage(), DamageEvent, nullptr, Asteroid);
			Asteroid->Destroy();
			Asteroid->ConditionalBeginDestroy();
		}
	}
}

float AEarth::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	// Reduce damage if it is a charger ship hitting the shield
	if (DamageCauser->IsA(AChargerShip::StaticClass()))
	{
		float DamageNegated = DamageAmount * (DamageReduction / 100.0f); //How much damage is reduced 
		DamageAmount -= DamageNegated;
	}
	if (bHasShield)
	{
		ShieldHealthCurrent -= DamageAmount;
		// if the shield is depleted, make an effect, disable the component (hide and remove collision)
		if (ShieldHealthCurrent <= 0.0f)
		{
			ShieldHealthCurrent = FMath::Clamp(ShieldHealthCurrent, 0.0f, ShieldHealthMax);
			bHasShield = false;
			ShieldMesh->SetNotifyRigidBodyCollision(bHasShield);
			ShieldMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			// TODO do a dissaperaing effect on the shield
			ShieldMesh->SetVisibility(false);

			EarthMesh->SetCollisionProfileName("IgnoreOnlyPawn");
		}
	}
	else
	{
		EarthHealthCurrent -= DamageAmount;
		if (EarthHealthCurrent <= 0.0f)
		{
			bEarthDestroyed = true;
		}
		ThisGameMode->IsEarthDestroyed(bEarthDestroyed);
	}
	ReceiveTakeDamage();

	return 0.0f;
}


// Aligning Asteroid to the level of the PlayerPawn
void AEarth::AlignBodyWithPlayersBody()
{
	FVector PlayerPosition = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	FVector CurrentPosition = GetActorLocation();
	CurrentPosition.Z = PlayerPosition.Z;
	SetActorLocation(CurrentPosition);
}

float AEarth::GetCurrentShieldHitPoints() const
{
	return ShieldHealthCurrent;
}

float AEarth::GetCurrentShieldHitPointsPercentage() const
{
	return ShieldHealthCurrent / ShieldHealthMax;
}

FText AEarth::GetCurrentShieldHitPointsIntText() const
{
	int32 ShieldHP = FMath::RoundHalfToZero(GetCurrentShieldHitPoints());
	FString ShieldHPString = FString::FromInt(ShieldHP);
	FText ShieldHPText = FText::FromString(ShieldHPString);
	return ShieldHPText;
}
