// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Earth.generated.h"

class UMeshComponent;
class USphereComponent;
class AAsteroidsGameMode;


UCLASS()
class ASTEROIDS_API AEarth : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEarth();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	
	// The Earth body component
	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* EarthMesh = nullptr;

		/* ***************************
		******************************

		Health  & Shield Health system

		******************************
		*************************** */

	UPROPERTY(EditAnywhere, Category = "Health")
	float EarthHealthMax;

	UPROPERTY(EditAnywhere, Category = "Health")
	float ShieldHealthMax;

	// Percentage of how much damage is reduced from Radial damage
	UPROPERTY(EditAnywhere, Category = "Damage", meta = (ClampMin = 0.0f, ClampMax	= 100.0f))
	float DamageReduction;

	bool bEarthDestroyed = false;

	UPROPERTY()
	AGameModeBase* GameModeBase = nullptr;

	UPROPERTY()
	AAsteroidsGameMode* ThisGameMode = nullptr;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);


protected:	
	void AlignBodyWithPlayersBody();

	UFUNCTION(BlueprintImplementableEvent, Category = "Damage")
	void ReceiveTakeDamage();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	UStaticMeshComponent* ShieldMesh = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Health", meta = (ClampMin = 0.0f))
	float ShieldHealthCurrent;

	UPROPERTY(BlueprintReadOnly, Category = "Health", meta = (ClampMin = 0.0f))
	float EarthHealthCurrent;
	
	// making sure we don't reduce wrong damage
	UPROPERTY(BlueprintReadOnly, Category = "Health")
	bool bHasShield;

public:
	// Return Current Shield Health
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetCurrentShieldHitPoints() const;

	// Returns Current Shield Health as a percentage of Max Shield Health
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetCurrentShieldHitPointsPercentage() const;

	// Return Current Shield Health as TEXT
	UFUNCTION(BlueprintPure, Category = "Health")
	FText GetCurrentShieldHitPointsIntText() const;
};
