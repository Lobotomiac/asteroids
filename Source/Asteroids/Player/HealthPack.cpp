// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthPack.h"
#include "Player/PlayerPawn.h"

#include "Components/StaticMeshComponent.h"



// TODO No collision, create new trace channel, overlap only player, on overlap destroy, fill player hp
// TODO Rotate as  a health pack, last for N time

// Sets default values
AHealthPack::AHealthPack()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HPMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HPMesh"));
	HPMesh->SetNotifyRigidBodyCollision(false);
	HPMesh->bGenerateOverlapEvents = true;
	HPMesh->OnComponentBeginOverlap.AddDynamic(this, &AHealthPack::OnOverlapBegin);
	HPMesh->SetCollisionProfileName(TEXT("PlayerPowerUp"));
	HPMesh->SetRelativeScale3D(FVector(0.5f, 0.5f, 0.5f));
	RootComponent = HPMesh;

	HealAmount = -30.0f;
}

// Called when the game starts or when spawned
void AHealthPack::BeginPlay()
{
	Super::BeginPlay();

	// Align with player
	FVector PlayerLocation = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	FVector CurrentLocation = GetActorLocation();
	CurrentLocation.Z = PlayerLocation.Z;
	SetActorLocation(CurrentLocation);
}

void AHealthPack::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		if (OtherActor->IsA(APlayerPawn::StaticClass()) && OtherComp->IsA(UStaticMeshComponent::StaticClass()))
		{
			//  Destroy itself
			// TODO Fix up ReceivePickedUp (figure out if it's needed in the 1st place)
			ReceivePickedUp();
			APlayerPawn* PlayerPawn = Cast<APlayerPawn>(OtherActor);
			if (PlayerPawn)
			{
				FDamageEvent DamageEvent;
				PlayerPawn->TakeDamage(GetHealAmount(), DamageEvent, 0, this);
			}
			Destroy();
		}
	}
}

float AHealthPack::GetHealAmount() const
{
	return HealAmount;
}



