// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HealthPack.generated.h"


class UStaticMeshComponent;

UCLASS()
class ASTEROIDS_API AHealthPack : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHealthPack();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Mesh body of the HealthPack
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* HPMesh = nullptr;
	
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(EditAnywhere, Category = "Health")
	float HealAmount;

	UFUNCTION(BlueprintImplementableEvent)
	void ReceivePickedUp();
public:	
// Heal Amount Getter
	UFUNCTION()
	float GetHealAmount() const;
	
};
