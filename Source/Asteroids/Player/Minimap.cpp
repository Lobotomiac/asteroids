// Fill out your copyright notice in the Description page of Project Settings.

#include "Minimap.h"
#include "Player/Earth.h"
#include "Enemy/Asteroid.h"

#include "CanvasItem.h"
#include "Engine/Canvas.h"
#include "EngineUtils.h"
#include "GameFramework/Actor.h"


void AMinimap::BeginPlay()
{
	// Set the timer for finding all AAsteroids spawned, which allows to display them on the minimap itself
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(PositionTimerHandle, this, &AMinimap::FindAllAsteroids, 2.5f, true, 1.0f);

	// Just making sure it doesn't iterate through them all the time
	for (AEarth* EarthIterator : TActorRange<AEarth>(GetWorld()))
	{
		Earth = EarthIterator;
	}
}

void AMinimap::DrawHUD()
{
	Super::DrawHUD();

	DrawRadar();
	DrawEarthOnRadar();
	DrawActorsOnRadar();
}


// Draws the earth on radar (basically just drawing a center piece on the map
void AMinimap::DrawEarthOnRadar()
{
	FVector2D RadarCenter = GetRadarCenterPosition();
	DrawRect(FLinearColor::Blue, RadarCenter.X, RadarCenter.Y, DrawPixelSize, DrawPixelSize);
}
 
// Returns the center of the drawn radar 
FVector2D AMinimap::GetRadarCenterPosition()
{
	// Depending on if there is a Canvas in the first place we return either the radars central position or a default TopLeft (0,0) position
	return (Canvas) ? FVector2D(Canvas->SizeX * RadarStartLocation.X, Canvas->SizeY * RadarStartLocation.Y) : FVector2D(0.0f, 0.0f);
}

// Draws the radar if the name isn't clear enough :P 
void AMinimap::DrawRadar()
{
	FVector2D RadarCenter = GetRadarCenterPosition();

	for (float LineAngle = 0.0f; LineAngle < 360; LineAngle += DegreeStep)
	{
		//We want to draw a circle in order to represent our radar
		//In order to do so, we calculate the sin and cos of almost every degree
		//It it impossible to calculate each and every possible degree because they are infinite
		//Lower the degree step in case you need a more accurate circle representation

		// We multiply our coordinates by radar size
		// In order to draw a circle with radius equal to the one we will play with through the editor
		float FixedX = FMath::Cos(LineAngle) * RadarRadius;
		float FixedY = FMath::Sin(LineAngle) * RadarRadius;

		// Actually draw 
		DrawLine(RadarCenter.X, RadarCenter.Y, RadarCenter.X + FixedX, RadarCenter.Y + FixedY, FColor::White, 1.0f);
	}
}

//  Iterate through too heavy to do a big raycast
void AMinimap::FindAllAsteroids()
{
	// Find first null pointer in array 
	// Change that pointer to the newly found Asteroid
	// else Add unique 

	// Find all instances of Asteroids 
	for (AAsteroid* Asteroid : TActorRange<AAsteroid>(GetWorld()))
	{
		//  If we do NOT find the current asteroid
		if (RadarActors.Find(Asteroid) < 0)
		{
			// Get the Index of the first element to be both a nullptr and invalid (to make sure we get a suitable pointer to swap
			int32 NullptrIndex = RadarActors.Find(nullptr && !IsValidLowLevel());
			// In case we didn't find any suitable pointers in the array (previously destroyed asteroids) add another entry to the array, or else swap a destroyed Asteroid for a newly spawned one
			if (NullptrIndex < 0)
			{
				RadarActors.AddUnique(Asteroid);
			}
			else
			{
				RadarActors[NullptrIndex] = Asteroid;
			}
		}
	}
	
	GEngine->ForceGarbageCollection();
}


// Change the world space to local space
FVector2D AMinimap::ConvertWorldLocationToLocal(AActor* ActorToPlace)
{
	if (Earth && Earth->IsValidLowLevel() && ActorToPlace && ActorToPlace->IsValidLowLevel())
	{
		// Convert World location to local, based on the transform of the Earth
		FVector ActorsLocal3DVector = Earth->GetTransform().InverseTransformPosition(ActorToPlace->GetActorLocation());

		// Rotate the vector by 90 degrees counter-clockwise in order to have a valid rotation in our radar (I have 0 clue what this means)
		ActorsLocal3DVector = FRotator(0.0f, -90.0f, 0.0f).RotateVector(ActorsLocal3DVector);

		// Apply the given distance scale
		ActorsLocal3DVector /= RadarDistanceScale;

		// Return a 2D vector based on the 3 vector we've created above
		return FVector2D(ActorsLocal3DVector);
	}
	return FVector2D(0.0f, 0.0f);
}


// Put the actors we'd like to the radar screen
void AMinimap::DrawActorsOnRadar()
{
	FVector2D RadarCenter = GetRadarCenterPosition();
	 /* Doing the same code as bellow for other actors 
	// but figured to have it just draw with a different colour
	// and for the lack of ability and/or will I'll just do it again here (2ce is still good enough)
	*/
	// Draw the PlayerPawn position on radar
	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	FVector2D PlayerPosition = ConvertWorldLocationToLocal(PlayerPawn);
	FVector TempVector = FVector(PlayerPosition.X, PlayerPosition.Y, 0.0f);
	TempVector = TempVector.GetClampedToMaxSize2D(RadarRadius - DrawPixelSize);
	PlayerPosition.X = TempVector.X;
	PlayerPosition.Y = TempVector.Y;
	DrawRect(FLinearColor::Green, RadarCenter.X + PlayerPosition.X, RadarCenter.Y + PlayerPosition.Y, DrawPixelSize, DrawPixelSize);

	// Go through all the Actors in the Array and draw them
	for (auto RadarActor : RadarActors)
	{
		// If the current Actor to be drawn was actually destroyed and awaiting to be GarbageCollected, do not draw them
		if (RadarActor && !RadarActor->IsActorBeingDestroyed())
		{
			FVector2D ConvertedLocation = ConvertWorldLocationToLocal(RadarActor);

			// Clamping the location of our actors to make sure we display them in the radar
			// To do that I've created a temporary vector in order to acces GetClampedToMaxSize2d function.
			// This function returns a clamped vector (if needed) to match our max length
			FVector TempVector = FVector(ConvertedLocation.X, ConvertedLocation.Y, 0.0f);

			// Subtract the pixel sizein order to make the radar display more acurate
			TempVector = TempVector.GetClampedToMaxSize2D(RadarRadius - DrawPixelSize);

			// Assign the converted X and Y values to the vector we want to display
			ConvertedLocation.X = TempVector.X;
			ConvertedLocation.Y = TempVector.Y;
			DrawRect(FLinearColor::Red, RadarCenter.X + ConvertedLocation.X, RadarCenter.Y + ConvertedLocation.Y, DrawPixelSize, DrawPixelSize);
		}	
	}
}