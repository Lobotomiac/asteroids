// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Minimap.generated.h"

class AEarth;

/**
 * 
 */
UCLASS()
class ASTEROIDS_API AMinimap : public AHUD
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	virtual void DrawHUD() override;

	// Where we draw our radar
	UPROPERTY(EditAnywhere, Category = "Radar")
	FVector2D RadarStartLocation = FVector2D(0.9f, 0.2f);

	// Size of our radar
	UPROPERTY(EditAnywhere, Category = "Radar")
	float RadarRadius = 100.0f;
	
	// With this we will control how detailed the drawn circle is
	UPROPERTY(EditAnywhere, Category = "Radar")
	float DegreeStep = 0.25f;

	// The size of drawn objects on the radar
	UPROPERTY(EditAnywhere, Category = "Radar")
	float DrawPixelSize = 5.0f;

	// Draws the Earth on the Radar
	void DrawEarthOnRadar();

	bool bGotEarth = false;

	UPROPERTY()
	AEarth* Earth = nullptr;

	// How far will our raycast be going out from earth to track objects
	UPROPERTY(EditAnywhere, Category = "Radar")
	float SphereRadius = 10000.0f;

	//How high we want our sphere to be
	UPROPERTY(EditAnywhere, Category = "Radar")
	float SphereHeight = 100.0f;

	// An array of actors we will fill and display on the radar
	UPROPERTY()
	TArray<AActor*> RadarActors;

	// The distance scale of the radar Actors (figure out what this is exactly)
	UPROPERTY(EditAnywhere, Category = "Radar")
	float RadarDistanceScale = 25.0f;

	// Converts the given actors world location to local (based on the earth)
	FVector2D ConvertWorldLocationToLocal(AActor* ActorToPlace);

	// Draws the Spawned actors in our radar if they are 
	void DrawActorsOnRadar();


private:
	// Timer handle for getting all actors positions
	FTimerHandle PositionTimerHandle;


	// Returns the center of the radar as a 2D vector
	FVector2D GetRadarCenterPosition();

	// Draws the radar
	void DrawRadar();

	// Iterate over enemies to locate them for drawing on radar
	void FindAllAsteroids();

};
