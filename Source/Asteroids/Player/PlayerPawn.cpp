// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.


#include "PlayerPawn.h"
#include "AsteroidsProjectile.h"
#include "Enemy/Asteroid.h"
#include "AsteroidsGameMode.h"
#include "MapBorderVolume.h"
#include "Player/HealthPack.h"

#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/GameMode.h"
#include "Materials/MaterialInstanceDynamic.h"



const FName APlayerPawn::MoveForwardBinding("MoveForward");
const FName APlayerPawn::MoveRightBinding("MoveRight");
const FName APlayerPawn::FireForwardBinding("FireForward");
const FName APlayerPawn::FireRightBinding("FireRight");
const FName APlayerPawn::SpeedBoostBinding("SpeedBoost");


/* TODO
	** if not invulnerable
	** pressing Shift (or other button) provides a boost to movement speed by a factor
	** Usable again after n seconds
	*/

APlayerPawn::APlayerPawn()
{	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShipMesh(TEXT("/Game/Meshes/TwinStickUFO.TwinStickUFO"));
	// Create the mesh component
	ShipMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShipMesh"));
	RootComponent = ShipMeshComponent;
	ShipMeshComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	ShipMeshComponent->SetStaticMesh(ShipMesh.Object);
	ShipMeshComponent->SetNotifyRigidBodyCollision(true);

	
	// Cache our sound effect
	static ConstructorHelpers::FObjectFinder<USoundBase> FireAudio(TEXT("/Game/Audio/TwinStickFire.TwinStickFire"));
	FireSound = FireAudio.Object;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when ship does
	CameraBoom->TargetArmLength = 1500.f;
	CameraBoom->RelativeRotation = FRotator(-80.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;	// Camera does not rotate relative to arm

	

	/// Movement
	MoveSpeedBase = 1000.0f;
	MoveSpeedCurrent = MoveSpeedBase;
	SpeedBoostFactorDefault = 1.0f;
	SpeedBoostFactor = SpeedBoostFactorDefault;
	SpeedBoostFactorIncrease = 5.0f;
	TurnRateInterpolationSpeed = 1300.0f;
	BoostInterpolationSpeed = 500.0f;
	IdleInterpolation = 5.0f;
	MoveSpeedReduction = 2.0f;

	BoostDuration = 3.0f;
	BoostCooldown = 3.0f;
	bBoostAvailable = true;
	bIsBoostOnCooldown = false;

	/// Weapon
	GunOffset = FVector(90.f, 0.f, 0.f);
	FireRate = 0.3f;
	bCanFire = true;

	/* **************************************************************************************
	CapsuleComponent to detect contact / overlap
	************************************************************************************** */
	ContactCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ContactCapsule"));
	ContactCapsule->InitCapsuleSize(CapsuleRadius, HalfHeight);
	ContactCapsule->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	ContactCapsule->SetupAttachment(RootComponent);
	ContactCapsule->OnComponentBeginOverlap.AddDynamic(this, &APlayerPawn::OnOverlapBegin); // Remove , Change to OnHit for mesh (perhaps leave to pick up powerups etc?


	/// Material Instance Dynamic setup
	EmissionValue = 0.0f;
	OscillationSpeedMultiplier = 10.0f;
	

	/// Damage
	MaxHitPoints = 100.0f;
	CollisionDamage = 30.0f;
	InvulnerabilityDuration = 3.0f;
	HitImpulseStrength =  550.0f;
}


void APlayerPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	// set up gameplay key bindings
	PlayerInputComponent->BindAxis(MoveForwardBinding);
	PlayerInputComponent->BindAxis(MoveRightBinding);
	PlayerInputComponent->BindAxis(FireForwardBinding);
	PlayerInputComponent->BindAxis(FireRightBinding);

	PlayerInputComponent->BindAction(SpeedBoostBinding, IE_Pressed, this, &APlayerPawn::MovementSpeedBoost);
}


void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	// Setting the Current Hit points to the maximum value for the Start of the game
	CurrentHitPoints = MaxHitPoints;
	// Cache the values to reduce constant load
	World = GetWorld();
	GameModeBase = World->GetAuthGameMode();
	ThisGameMode = Cast<AAsteroidsGameMode>(GameModeBase);	

	// This code can't be in the constructor for some reason 
	MaterialInstance = ShipMeshComponent->CreateAndSetMaterialInstanceDynamic(0);
	if (MaterialInstance)
	{
		MaterialInstance->SetScalarParameterValue(FName("EmissiveColor"), EmissionValue);
	}
	// Making sure we start off in a nice and firm position, not moving anywhere randomly
	SmoothMovement = FVector(0.0f, 0.0f, 0.0f);
}


void APlayerPawn::Tick(float DeltaSeconds)
{
	// TODO Maybe figure out how to create a bump back effect when hitting other object

	// Calculate  movement
	const FVector Movement = GetMovement(DeltaSeconds);
	// Keeping track of previous tick movement
	
	ReceivePlayerMovingSound();

	// Get the Pawn Rotation, Update if needed and smooth it out to not be jerky but a smooth(ish) turn when changing rotation
	const FRotator CurrentRotation = GetActorRotation();
	const FRotator NewRotation = Movement.Rotation();

	FHitResult Hit(1.f);

	static FRotator NewRotationSmooth;
	if (Movement.SizeSquared() > 0.0f)		
	{
		SmoothMovement = FMath::VInterpConstantTo(SmoothMovement, Movement, DeltaSeconds, BoostInterpolationSpeed);
		NewRotationSmooth = FMath::RInterpConstantTo(CurrentRotation, NewRotation, DeltaSeconds, TurnRateInterpolationSpeed);
	}
	else
	{
		SmoothMovement = FMath::VInterpConstantTo(SmoothMovement, Movement, DeltaSeconds, IdleInterpolation);
	}

	RootComponent->MoveComponent(SmoothMovement, NewRotationSmooth, true, &Hit);

	if (Hit.IsValidBlockingHit())
	{
			const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
			const FVector Deflection = FVector::VectorPlaneProject(Movement, Normal2D) * (1.f - Hit.Time);
			RootComponent->MoveComponent(Deflection, NewRotationSmooth, true);
	}
		
	// Create fire direction vector
	const float FireForwardValue = GetInputAxisValue(FireForwardBinding);
	const float FireRightValue = GetInputAxisValue(FireRightBinding);
	const FVector FireDirection = FVector(FireForwardValue, FireRightValue, 0.f);

	// Try and fire a shot
	FireShot(FireDirection);
}


FVector APlayerPawn::GetMovement(float DeltaSeconds)
{
	// Find movement direction
	const float ForwardValue = GetInputAxisValue(MoveForwardBinding);
	const float RightValue = GetInputAxisValue(MoveRightBinding);

	// Clamp max size so that (X=1, Y=1) doesn't cause faster movement in diagonal directions
	const FVector MoveDirection = FVector(ForwardValue, RightValue, 0.f).GetClampedToMaxSize(1.0f);

	// return Movement
	return MoveDirection * MoveSpeedCurrent * DeltaSeconds * SpeedBoostFactor;
}


// TODO Remove OnOverlapBegins, transfer functionality to AAsteroid since thats the only things it's doing anyway

// called when overlapping other actors
void APlayerPawn::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (bCanBeDamaged)
	{
		// In case of impact with an Asteroid take damage
		if (OtherActor)
		{
			if (OtherActor != this && OtherComp && OtherComp->IsSimulatingPhysics())
			{
				OtherComp->AddImpulseAtLocation(HitImpulseStrength * GetMovement(World->GetDeltaSeconds()), GetActorLocation()); // push Physics simulating stuff away
			}
			else if (OtherActor != this && OtherActor->IsA(AMapBorderVolume::StaticClass()))
			{
				PlayerInvulnerability(World);
			}
		}
	}
}


// Shooting mechanics
void APlayerPawn::FireShot(FVector FireDirection)
{
	// If it's ok to fire again
	if (bCanFire == true)
	{
		// If we are pressing fire stick in a direction
		if (FireDirection.SizeSquared() > 0.0f)
		{
			const FRotator FireRotation = FireDirection.Rotation();
			// Spawn projectile at an offset from this pawn
			const FVector SpawnLocation = GetActorLocation() + FireRotation.RotateVector(GunOffset);
			const FActorSpawnParameters SpawnParams;
			if (World != NULL)
			{
				// spawn the projectile
				World->SpawnActor<AAsteroidsProjectile>(ProjectileBlueprint, SpawnLocation, FireRotation, SpawnParams);
			}
			bCanFire = false;
			World->GetTimerManager().SetTimer(TimerHandle_ShotTimerExpired, this, &APlayerPawn::ShotTimerExpired, FireRate);

			// try and play the sound if specified 
			ReceiveFireShot();
			bCanFire = false;
		}
	}
}


// Player able to shoot again
void APlayerPawn::ShotTimerExpired()
{
	bCanFire = true;
}


float APlayerPawn::GetCurrentHitPoints() const
{
	return CurrentHitPoints;
}


float APlayerPawn::GetCurrentHitPointsPercentage() const
{
	return CurrentHitPoints / MaxHitPoints;
}

// Used for displaying the hp widget BP in UE editor
FText APlayerPawn::GetCurrentHitPointsIntText() const
{
	int32 HP = FMath::RoundHalfToZero(GetCurrentHitPoints());
	FString HPString = FString::FromInt(HP);
	FText HPText = FText::FromString(HPString);
	return HPText;
}


void APlayerPawn::UpdateCurrentHitPoints(float HitPointChange)
{
	CurrentHitPoints += HitPointChange;
	CurrentHitPoints = FMath::Clamp(CurrentHitPoints, 0.0f, MaxHitPoints);
}


float APlayerPawn::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	// In case of picking up a HealthPack, avoid the rest of the damage mechanism
	UpdateCurrentHitPoints(-DamageAmount);
	if (DamageAmount < 0.0f)
	{
		ReceiveHealing();
		return 0.0f;
	}

	if (DamageShake)
	{
		World->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(DamageShake, 1.0f);
		if (BoostShake)
		{
			World->GetFirstPlayerController()->PlayerCameraManager->StopAllInstancesOfCameraShake(BoostShake);
		}
	}
	
	PlayerInvulnerability(World);
	ThisGameMode->IsPlayerDead();
	ReceiveTakeDamage();
	return DamageAmount;
}


void APlayerPawn::PlayerInvulnerability(UWorld* World)
{
	// for how long is the player invulnerable
	if (World)
	{
		World->GetTimerManager().SetTimer(TimerHandle_InvulnerabilityExpired, this, &APlayerPawn::InvulnerabilityTimerExpired, InvulnerabilityDuration);
		World->GetTimerManager().SetTimer(TimerHandle_MaterialEmissionColorStopped, this, &APlayerPawn::WhilePlayerInvulnerable, World->GetDeltaSeconds());
	}
	ShipMeshComponent->SetCollisionProfileName(TEXT("Invulnerable"));

	// Making the player unable to boost while invulnerable
	bBoostAvailable = false;
	
	// Reducing the movement speed to simulate damage
	SpeedBoostFactor = SpeedBoostFactorDefault;
	MoveSpeedCurrent = MoveSpeedBase / MoveSpeedReduction;
	bCanBeDamaged = false;
}


void APlayerPawn::WhilePlayerInvulnerable()
{
	// Oscilating between 0 and 1 to change the material color of the pawn indicating invulnerability
	const float DeltaSeconds = World->GetDeltaSeconds();
	EmissionValue = 0.5 + (FMath::Sin(World->GetRealTimeSeconds() * OscillationSpeedMultiplier) / 2);
	if (MaterialInstance)
	{
		MaterialInstance->SetScalarParameterValue(FName("EmissiveColor"), EmissionValue);
		ShipMeshComponent->SetMaterial(0, MaterialInstance);
	}

	// increasing the movement speed 
	MoveSpeedCurrent = FMath::Clamp((MoveSpeedCurrent + MoveSpeedCurrent * DeltaSeconds), 0.0f, MoveSpeedBase);

	// calling itself as long as we are invulnerable
	if (!bCanBeDamaged)
	{
		World->GetTimerManager().SetTimer(TimerHandle_MaterialEmissionColorStopped, this, &APlayerPawn::WhilePlayerInvulnerable, World->GetDeltaSeconds());
	}
	else if(MaterialInstance && bCanBeDamaged)
	{
		EmissionValue = 0.0f;
		MaterialInstance->SetScalarParameterValue(FName("EmissiveColor"), EmissionValue);
		ShipMeshComponent->SetMaterial(0, MaterialInstance);
	}
}


void APlayerPawn::InvulnerabilityTimerExpired()
{
	// Enables incoming damage
	bCanBeDamaged = true;

	// Keep Boost disabled if the cooldown hasn't finished
	if (!bIsBoostOnCooldown)
	{
		bBoostAvailable = true;
	}
	
	// mesh can collide with objects again
	ShipMeshComponent->SetCollisionProfileName(TEXT("Pawn"));
	
	// Resetting movement speed
	MoveSpeedCurrent = MoveSpeedBase;
}

// Boost to the movement speed by a factor of n
void APlayerPawn::MovementSpeedBoost()
{
	if (bBoostAvailable && bCanBeDamaged && !bIsBoostOnCooldown)
	{
		bBoostAvailable = false;
		bIsBoostOnCooldown = true;
		// Speed Player up
		SpeedBoostFactor = SpeedBoostFactorIncrease;	
		// Set the Duration and Cooldown timer
		World->GetTimerManager().SetTimer(TimerHandle_MoveSpeedBoostExpired, this, &APlayerPawn::MovementSpeedBoostDurationAndCooldown, BoostDuration);
		if (BoostShake)
		{
			World->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(BoostShake, 1.0f);
		}
		
		ReceiveMovementSpeedBoost();
	}
}

// Handles the MovementBoost ability Duration and Enabling it after the cooldown has passed
void APlayerPawn::MovementSpeedBoostDurationAndCooldown()
{
	// call timer again to count down the cooldown, enable boost again after
	World->GetTimerManager().SetTimer(TimerHandle_MoveSpeedBoostExpired, this, &APlayerPawn::MovementSpeedBoostDurationAndCooldown, BoostCooldown);
	if (SpeedBoostFactor <= 2.0f)
	{
		bBoostAvailable = true;
		// Cooldown  has passed, able to use it again 
		bIsBoostOnCooldown = false;
		World->GetTimerManager().ClearTimer(TimerHandle_MoveSpeedBoostExpired);
	}

	// stop boost ( return factor to original value)
	SpeedBoostFactor = SpeedBoostFactorDefault;
}