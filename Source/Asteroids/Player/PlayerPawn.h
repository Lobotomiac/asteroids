// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerPawn.generated.h"

class UCapsuleComponent;
class AAsteroidsGameMode;
class AAsteroidsProjectile;

// TODO fix player not receiving damage from ChargerShip explosion (if ChargerShip explodes on impact with other objects)

UCLASS(Blueprintable)
class APlayerPawn : public APawn
{
	GENERATED_BODY()

	/* The mesh component */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* ShipMeshComponent = nullptr;

	/** The camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent = nullptr;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom = nullptr;

	// a component for detecting collision 
	UPROPERTY(VisibleAnywhere, Category = "Contact Capsule")
	UCapsuleComponent* ContactCapsule = nullptr;

protected:
	APlayerPawn();

	/** Offset from the ships location to spawn projectiles */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	FVector GunOffset;
	
	/* How fast the weapon will fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float FireRate;


	/* *****************
	** Movement Setup **
	***************** */


	/* The speed our ship moves around the level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float MoveSpeedBase;

	/* The speed our ship moves around the level after receiving damage */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float MoveSpeedCurrent;

	// Movement speed factor, basically movement speed multiplier 
	UPROPERTY()
	float SpeedBoostFactor;

	UPROPERTY()
	float SpeedBoostFactorDefault;

	FVector SmoothMovement;

	// By how much the speed boost factor increases 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float SpeedBoostFactorIncrease;

	// For how long is the speed boost active
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float BoostDuration;

	// For how long is the speed boost on cooldown (unusable)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float BoostCooldown;

	// How fast will the boost accelerate and decelerate movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float BoostInterpolationSpeed;

	// How much (or little) drag will the player have without giving movement input
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float IdleInterpolation;
	
	/* Factor of slowing down the player movement after damage */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float MoveSpeedReduction;

	/* How smooth the interpolation for body rotatino is */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float TurnRateInterpolationSpeed;

	FVector GetMovement(float DeltaSeconds);

	// Can the boost be used
	bool bBoostAvailable;

	// Has the boost cooldown passed
	bool bIsBoostOnCooldown;

	// Boost the players movement speed
	UFUNCTION()
	void MovementSpeedBoost();

	// Sets up how long the boost lasts and for how long it can't be used
	UFUNCTION()
	void MovementSpeedBoostDurationAndCooldown();

	// Blueprint implementable Additions to Functionality | Simplifying Sound and Particle implementation
	UFUNCTION(BlueprintImplementableEvent, Category = "Sound")
	void ReceiveMovementSpeedBoost();


	UFUNCTION(BlueprintImplementableEvent, Category = "Sound")
	void ReceivePlayerMovingSound();


	/* ************************
	** End of Movement Setup **
	************************ ** */

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	class USoundBase* FireSound;
	

	UPROPERTY(EditAnywhere, Category = "Contact Capsule")
	float CapsuleRadius = 51.0f;

	UPROPERTY(EditAnywhere, Category = "Contact Capsule")
	float HalfHeight = 55.0f; 

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Damage that PlayerPawn inflicts to objects it colides with
	UPROPERTY(EditAnywhere, Category = "Overlap")
	float CollisionDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Overlap")
	float HitImpulseStrength;

	// Begin Actor Interface
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	virtual void BeginPlay() override;
	// End Actor Interface

	/* Fire a shot in the specified direction */
	void FireShot(FVector FireDirection);

	// Signaling to Blueprints that the player shot a projectile
	UFUNCTION(BlueprintImplementableEvent, Category = "Sound")
	void ReceiveFireShot();


	/* Handler for the fire timer expiry */
	void ShotTimerExpired();

	/* Handler for the Invulnerability timer expiry */
	void InvulnerabilityTimerExpired();

	// Static names for Axis bindings
	static const FName MoveForwardBinding;
	static const FName MoveRightBinding;
	static const FName FireForwardBinding;
	static const FName FireRightBinding;

	// Static names for Action bindings
	static const FName SpeedBoostBinding;

	/*	******************************************************************************************
	The next part of the section is here for Health Points system and its HUD display 
	Rest of the private members will be dedicated to it to have them all grouped in the same place 
	******************************************************************************************* */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float MaxHitPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float CurrentHitPoints;

	UFUNCTION()
	void UpdateCurrentHitPoints(float HitPointChange);

	/* **************************************************
	End of HealthPoint system and its HUD display portion
	************************************************** */
	
	// Signaling to Blueprints to play a sound on taking damage (at least) Implemented in BP
	UFUNCTION(BlueprintImplementableEvent, Category = "Sound")
	void ReceiveTakeDamage();

	// Calling the BP to expand on Healing mechanism
	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveHealing();


	/* How long the player is invulnerable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float InvulnerabilityDuration;

	UFUNCTION()
	void PlayerInvulnerability(UWorld* World);

	/* We'll use this to shake the camera around when taking damage */
	UPROPERTY(EditAnywhere)
	TSubclassOf<UCameraShake> DamageShake;

	/* We'll use this to shake the camera around when taking damage */
	UPROPERTY(EditAnywhere)
	TSubclassOf<UCameraShake> BoostShake;

	UPROPERTY()
	UMaterialInstanceDynamic* MaterialInstance = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Material")
	float EmissionValue;

private:
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<AAsteroidsProjectile> ProjectileBlueprint;

	/* Flag to control firing  */
	uint32 bCanFire : 1;

	/** Handle for efficient management of ShotTimerExpired timer */
	FTimerHandle TimerHandle_ShotTimerExpired;

	/* Handle for managing invulnerability intervals (after receiving damage for example)*/
	FTimerHandle TimerHandle_InvulnerabilityExpired;

	/* Handle for Damage indication ( changing material at runtime) */
	FTimerHandle TimerHandle_MaterialEmissionColorStopped;

	/* Handle for stopping the speed boost, returning movement speed to normal */
	FTimerHandle TimerHandle_MoveSpeedBoostExpired;

	// Oscillation Speed multiplier for material change
	UPROPERTY(EditAnywhere, Category = "Material")
	float OscillationSpeedMultiplier; 

	UFUNCTION()
	void WhilePlayerInvulnerable();

	UPROPERTY()
	AGameModeBase* GameModeBase = nullptr;
	
	UPROPERTY()
	AAsteroidsGameMode* ThisGameMode = nullptr;

public:
	/** Returns ShipMeshComponent subobject **/
	FORCEINLINE class UStaticMeshComponent* GetShipMeshComponent() const { return ShipMeshComponent; }
	/** Returns CameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY()
	UWorld* World = nullptr;

	// Return Current HP
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetCurrentHitPoints() const;

	// Returns Current HP as a percentage of Max hp
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetCurrentHitPointsPercentage() const;

	// Return Current HP as TEXT
	UFUNCTION(BlueprintPure, Category = "Health")
	FText GetCurrentHitPointsIntText() const;

	UFUNCTION()
	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

};

