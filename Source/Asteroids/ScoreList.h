// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScoreList.generated.h"

/**
 * 
 */
UCLASS()
class ASTEROIDS_API UScoreList : public UUserWidget
{
	GENERATED_BODY()

protected:
	
	// This will Get the Blueprint to display a Score from the ScoreList array
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void ReceiveAddScoreToUI();

	UPROPERTY(BlueprintReadOnly)
	int32 ScoreToAdd;
	
	
public:

	// This will add the score into our UI by using ReceiveAddScoreToUI protected function (not sure I get what this means exactly? Just security perhaps?)
	void AddScoreToUI(int32 Score);
	
};
