// Fill out your copyright notice in the Description page of Project Settings.

#include "Sun.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ASun::ASun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SunStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SunMesh"));
	SunStaticMeshComponent->bGenerateOverlapEvents = false;
	SetActorEnableCollision(false);
	RootComponent = SunStaticMeshComponent;

	RotationRadius = 2000.0f;
	RotationAngle = 0.0f;
	RotationIncrement = 0.1f;

	bCanBeDamaged = false;
	bHidden = true;

}

// Called when the game starts or when spawned
void ASun::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector CenterLocation = FVector(-20.0f, -120.0f, 573.0f);

	FVector Radius = FVector(RotationRadius, 0.0f, 0.0f);

	RotationAngle += RotationIncrement;

	if (RotationAngle > 360.0f)
	{
		RotationAngle = 0.2f;
	}

	FVector RotateValue = Radius.RotateAngleAxis(RotationAngle, FVector(0.0f, 0.0f, 1.0f));

	CenterLocation += RotateValue;

	SetActorLocation(CenterLocation);
}

