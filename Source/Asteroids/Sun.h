// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sun.generated.h"


class UStaticMeshComponent;

UCLASS()
class ASTEROIDS_API ASun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASun();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Our physical representation (that we might use for something sometimes
	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* SunStaticMeshComponent = nullptr;

	// The degree of rotation
	float RotationAngle;

	// Rotation radius 
	UPROPERTY(EditAnywhere)
	float RotationRadius;

	// How much we rotate per frame
	UPROPERTY(EditAnywhere)
	float RotationIncrement;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
